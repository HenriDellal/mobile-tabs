function onError(error) {
	console.log("Error: ${error}")
}

function listTabs(tabs) {
	let tabsList = document.getElementById("tabs-list");
    let tabsFragment = document.createDocumentFragment();
	var activeTabId;
	tabsList.textContent = '';
	for (let tab of tabs) {
		let listElement = document.createElement('button');
		listElement.classList.add('switchTabs');
		listElement.setAttribute('id', tab.id);
		if (tab.active) {
			listElement.classList.add('activeTab');
			activeTabId = tab.id;
		}
		
		let tabIcon = document.createElement('img');
		tabIcon.classList.add('tabIcon');
		tabIcon.setAttribute('src', tab.favIconUrl);

		let tabLabel = document.createElement('div');
		tabLabel.classList.add('tabLabel');
		tabLabel.textContent = tab.title;
		
		let closeTabButton = document.createElement('button');
		closeTabButton.textContent = "'";
		closeTabButton.classList.add('closeTab');
		closeTabButton.setAttribute('id', tab.id);
		
		listElement.appendChild(tabIcon);
		listElement.appendChild(tabLabel);
		listElement.appendChild(closeTabButton);
		tabsFragment.appendChild(listElement);
	}
	tabsList.appendChild(tabsFragment);
	document.getElementById(activeTabId).scrollIntoView();
}

let querying = browser.tabs.query({currentWindow: true});
querying.then(listTabs, onError);

document.getElementById("createTab").textContent = '+';

document.addEventListener("click", (ev) => {
	var id;
	
	if (ev.target.classList.contains("switchTabs")) {
		id = +ev.target.getAttribute("id");
	} else if (ev.target.parentElement.className === "switchTabs" && !ev.target.classList.contains("closeTab")) {
		id = +ev.target.parentElement.getAttribute("id");
	}
	
	if (id) {
		browser.tabs.query({
			currentWindow: true
		}).then((tabs) => {
			for (var tab of tabs) {
				if (tab.id === id) {
					browser.tabs.update(id, {active: true});
					window.close();
				}
			}
		});
	} else if (ev.target.classList.contains("closeTab")) {
		id = +ev.target.getAttribute("id");
		browser.tabs.query({
			currentWindow: true
		}).then((tabs) => {
			for (let tab of tabs) {
				if (tab.id === id) {
					browser.tabs.remove(id)
					browser.tabs.query({currentWindow: true}).then(listTabs, onError);
				}
			}
			
		});
	} else if (ev.target.classList.contains("createTab")) {
		browser.tabs.create({});
		browser.tabs.query({currentWindow: true}).then(listTabs, onError);
	}

	ev.preventDefault();
});
