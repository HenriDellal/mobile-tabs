function updateCount() {
	browser.tabs.query({})
		.then((tabs) => {
			browser.browserAction.setBadgeText({text: tabs.length.toString()});
	});
}


browser.tabs.onRemoved.addListener(
	(tabId) => {
		updateCount();
	}
);

browser.tabs.onCreated.addListener(
	(tabId) => {
		updateCount();
	}
);

browser.browserAction.setBadgeBackgroundColor({color: "white"});
updateCount();
